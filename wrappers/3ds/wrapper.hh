#include <3ds.h>

int _main(int argc, char *argv[]);
class exitexc : public std::string {};
static int exitcode = 0;


void wait_for_gpkey(u32 key) {
    u32 kDown;
    while (true) {
        hidScanInput();
        kDown = hidKeysDown();
        if (kDown & key) {
            break;
        }
    }
}

int main(void) {
    using namespace std;
    // Intialise stuff
    gfxInitDefault();
    consoleInit(GFX_TOP, NULL);
    // Generate argc and argv
    char *argv[2] = {(char*)"/proc/self/exe", (char*)"/main.pil"};
    int argc = 1;
    ifstream f("/main.pil");
    if (f.good()) {
        argc = 2;
    }
    // Run actual main
    int res;
    try {
        res = _main(argc, argv);
    } catch (exitexc) {
        res = exitcode;
    }
    // Print message and wait for key to be pressed
    clog << endl << "Interpreter returned " << res << endl;
    clog << "Press START to exit" << flush;
    wait_for_gpkey(KEY_START);
    // Exit
    cerr << endl << "Error: exit is not yet implemented; will reboot" << flush;
    gfxExit();
}

static SwkbdState swkbd;
static char swkbd_buf[2048];
std::stringstream readkbd(std::string hint) {
    // Wait for 'A' key
    wait_for_gpkey(KEY_A);
    // Read input
    memset(swkbd_buf, 0, sizeof(swkbd_buf));
    swkbdInit(&swkbd, SWKBD_TYPE_NORMAL, 3, -1);
    swkbdSetHintText(&swkbd, hint.c_str());
    swkbdInputText(&swkbd, swkbd_buf, sizeof(swkbd_buf));
    // Echo input
    std::cout << swkbd_buf << std::endl;
    // Write input to stringstream
    std::stringstream out;
    out << swkbd_buf;
    return out;
}

void _exit(int code) {
    exitcode = code;
    throw exitexc();
}
