#include <nds.h>
#include <stdio.h>

int _main(int argc, char *argv[]);
class exitexc : public std::string {};
static int exitcode = 0;
static Keyboard *swkbd;

PrintConsole topScreen;
PrintConsole bottomScreen;


void kbfeedback(int key) {
    if (key > 0) printf("%c", key);
}

void wait_for_gpkey(u32 key) {
    u32 kDown;
    while (true) {
        scanKeys();
        kDown = keysDown();
        if (kDown & key) {
            break;
        }
    }
}

int main(void) {
    using namespace std;
    // Intialise stuff
    consoleDemoInit();
    swkbd = keyboardDemoInit();
    swkbd->OnKeyPressed = kbfeedback;
    // Generate argc and argv
    char *argv[2] = {(char*)"/proc/self/exe", (char*)"/main.pil"};
    int argc = 1;
    ifstream f("/main.pil");
    if (f.good()) {
        argc = 2;
    }
    // Run actual main
    int res;
    try {
        res = _main(argc, argv);
    } catch (exitexc) {
        res = exitcode;
    }
    // Print message and wait for key to be pressed
    clog << endl << "Interpreter returned " << res << endl;
    clog << "Press START to exit" << flush;
    wait_for_gpkey(KEY_START);
    // Exit
    return res;
}

std::stringstream readkbd(std::string hint) {
    std::string outstr;
    std::getline(std::cin, outstr);
    std::stringstream out;
    out << outstr;
    return out;
}

void _exit(int code) {
    exitcode = code;
    throw exitexc();
}
