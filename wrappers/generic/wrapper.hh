int _main(int argc, char *argv[]);

int main(int argc, char *argv[]) {
    return _main(argc, argv);
}

std::stringstream readkbd(std::string hint) {
    std::string outstr;
    std::getline(std::cin, outstr);
    std::stringstream out;
    out << outstr;
    return out;
}

inline void _exit(int code) {
    exit(code);
}
