all: generic

generic:
	@mv Makefile Makefile.bak
	@mv Makefile-generic Makefile
	@make | cat
	@mv Makefile Makefile-generic | cat
	@mv Makefile.bak Makefile | cat

3ds:
	@mv Makefile Makefile.bak
	@mv Makefile-3ds Makefile
	@make | cat
	@mv Makefile Makefile-3ds | cat
	@mv Makefile.bak Makefile | cat

nds:
	@mv Makefile Makefile.bak
	@mv Makefile-nds Makefile
	@make | cat
	@mv Makefile Makefile-nds | cat
	@mv Makefile.bak Makefile | cat

clean:
	@mv Makefile Makefile.bak
	@mv Makefile-nds Makefile
	@make clean | cat
	@mv Makefile Makefile-nds
	@mv Makefile-3ds Makefile
	@make clean | cat
	@mv Makefile Makefile-3ds
	@mv Makefile-generic Makefile
	@make clean | cat
	@mv Makefile Makefile-generic
	@mv Makefile.bak Makefile | cat
